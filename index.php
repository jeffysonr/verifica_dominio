<html>
	<head>
		<title>Verificando a disponibilidade de um dominio</title>
	</head>

	<body>

		<form id="frm" action="verifica.php" method="get" >

			Dominio: <input type="text" name="dominio" id="dominio" />

			<select name="extensao" id="extensao">
			    <option value="com.br">.com.br</option>
			    <option value="com">.com</option>
			    <option value="net">.net</option>
			</select>

			<input type="submit" value="ok">
		</form>

		<div id="result"></div>

		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#frm").submit(function(){
					var dominio = $("#dominio").val();
					var extensao = $("#extensao").val();

					if (dominio) {
						$('#result').load('verifica.php?dominio='+dominio+'&extensao='+extensao);
					} else {
						alert("Por favor, informe seu dominio");
						$("#dominio").focus();
					}

					return false;			
				});
			});
		</script>
	</body>
</html>