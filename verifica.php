<?php
	include("domain.class.php");

	$dominio = (isset($_REQUEST["dominio"]) && !empty($_REQUEST["dominio"])) ? $_REQUEST["dominio"] : "";

	if ($dominio)
		$resposta = Domain::isAvailable($dominio, $_REQUEST["extensao"]);
	else
		$resposta = "Por favor informe seu dominio.";

	$cor = (eregi("registrado", $resposta)) ? "#d60404" : "#23ba05";
?>

<span <?php echo "style='color:{$cor}'"?>><?php echo $resposta; ?><span>
