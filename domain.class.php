
<?php

/**
 * WARRANTY AND LICENSE
 *
 * THIS SOFTWARE(CLASS) AND OTHER INFORMATION
 * ARE PROVIDED "AS IS" WITH ALL FAULTS.
 * I DO NOT WARRANT AND CERTIFICATE AUTHORITIES
 * AND I CAN NOT WARRANT THE PERFORMANCE
 * OR RESULTS YOU MAY OBTAIN BY USING THIS SOFTWARE(CLASS)
 *
 * THIS SOFTWARE IS FREE TO USE ON PERSONAL
 * OR COMMERCIAL USE, CONSIDERING THE ABOVE WARRANTY.
 * YOU CAN USE IT, MODIFY WITH  NO RESTRICTIONS.
 * IF YOU UPDATE OR MODIFY THE CLASS,
 * I WOULD APPRECIATE IF YOU COULD
 * SEND ME THE UPDATES TO THE BELOW
 * CONTACT EMAIL.
 *
 * FEEDBACK IS ALSO DESIRED,
 * SO FEEL FREE TO CONTACT ME TOO
 * AT THE SAME EMAIL ADDRESS
 *
 * @package     Domain
 * @version     1.0
 * @author      Gilberto albino
 * @contact     www@gilbertoalbino.com
 */



/**
 * In the case the server
 * delays to serve the result
 * let's zero out the php time limit
 */

set_time_limit(0);



/**
 * This class can check via Curl
 * if a domain is available for registration
 * It checks if the class is for
 * Registro.br check or for Whois
 *
 * The class performs some
 * string matching from
 * the returned result from Curl
 * and it can determine if
 * the domain is available or not.
 *
 */

class Domain {



    /**
     * The domain to be checked
     *
     * @var String
     */

    public static $domain;



    /**
     * The
     * @var String
     */

    public static $extension;





    /**
     * The types used for whois
     *
     * @note Modify to suit your needs
     * @var Array
     */

    public static $whoisExtensions = array(

        'com',

        'net',

        // add any other you need

    );



    public static $isBr;

    public static $url;

    public static $referer;

    public static $postFields;



    /**
     * This method do some steps
     * and verifies if the search is for
     * registro.br or for whois
     *
     * @param String $domain
     * @param String $extension
     * @return Boolean
     */

    public static function check( $domain, $extension ) {



        self::$domain = $domain;

        self::$extension = $extension;



        /**
         * Helpers to test if the
         * domain and type are not empty
         */

        $hasExtension = ( !empty ( $extension ) ) ? true : false;

        $hasDomain = ( !empty ( $domain ) ) ? true : false;



        /**
         * The $extension and $domain can not be empty
         */

        if( $hasDomain == false && $hasExtension == false ) {

            return 'You must inform the domain and extension';

        } else {



            if( in_array( $extension, self::$whoisExtensions ) ) {

                self::$isBr = false;

            } else {

                self::$isBr = true;

            } // end of if in_array



        } // end of if $hasDomain and $hasExtension



    } // end of check method





    /**
     * This method filters the
     * result string and parses
     * the matching string
     *
     * @see isAvailable
     * @return String?
     */

    public static function grabInformationFromString( $string ) {



        /**
         * This texts could be generated
         * from true or false if returned
         * from this method.
         * I've opted here to put the text
         * to hide it from the main application
         */

        if( self::$isBr == true ) {

            /**
             * Registro.BR uses html entities
             * so take care not to use a string
             * without them!
             */

            if ( preg_match("#(dispon&iacute;vel para registro)#", $string ) ) {

                $output = "O domínio <b>"

                    . self::$domain . '.' . self::$extension

                    . "</b> está disponível para registro"

                ;

            } else {

                $output = "O domínio <b>"

                    . self::$domain . '.' . self::$extension

                    . "</b> já está registrado"

                ;

            } // end of registro.br preg_match



        } else {

            /**
             * Every internation domain name
             * has a Creation Date
             * so it's the most secure
             * way to match
             */

            if ( preg_match("#(Creation Date)#", $string ) ) {

                $output = "O domínio <b>"

                    . self::$domain . '.' . self::$extension

                    . "</b> já está registrado"

                ;

            } else {

                $output = "O domínio <b>"

                    . self::$domain . '.' . self::$extension

                    .  "</b> está disponível para registro"

                ;

            } // end of whois preg_match

        }



        return $output;



    } // end of grabInformationFromString





    /**
     * This method returns
     * if the domain is available
     *
     * @see check()
     * @return Boolean
     */

    public static function isAvailable( $domain, $extension ) {



        self::check( $domain, $extension );



        if( self::$isBr == true ) {

          $result = self::checkFromRegistroBR();

        } else {

          $result = self::checkFromWhois();

        }



        return $result;



    } // end of isAvailable method





    /**
     * This method returns
     * if the domain is online
     * or offline
     *
     * @return Boolean
     */

    public static function isOnline() {

        /**
         * @todo
         */

    } // end of isOnline method





    /**
     * This method uses CURL to connect
     * to a given server
     *
     * @return String
     */

    public static function useCurl() {



        /**
         * Whois requires the user agent,
         * so to enforce the use of it
         * CURLOPT_USERAGENT uses Mozilla
         */

        $userAgent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";



        /**
         * Inicializes a Curl connection
         */

        $ch = curl_init();



        /**
         * Set curl options
         */

        curl_setopt( $ch, CURLOPT_USERAGENT, $userAgent);

        /**
         * The  url is passed in the proper method
         * @see checkFromRegistroBR() and checkFromWhois()
         */

        curl_setopt( $ch, CURLOPT_URL, self::$url );

        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );

        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );



        /**
         * The referer is passed in the proper method
         */

        curl_setopt( $ch, CURLOPT_REFERER, self::$referer );

        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );

        /**
         * These fields are passed in the proper method
         */

        curl_setopt( $ch, CURLOPT_POSTFIELDS, self::$postFields );



        //curl_setopt( $ch, CURLOPT_POST, 1 );



        /**
         * The content to be parsed
         * within grabInformationFromString()
         * @see grabInformatinFromString()
         */

        $content = curl_exec($ch);



        /**
         * Closes the Curl connection
         */

        curl_close( $ch );



        return $content;



    } // end of useCurl





    /**
     * This method connecs to registro.BR server
     *
     * @see useCurl()
     * @return String
     */

    public static function checkFromRegistroBR() {



        /**
         * Values required for useCurl()
         * @see useCurl()
         */



        self::$postFields = null;

        self::$postFields = "qr=" . self::$domain . '.' . self::$extension;



        self::$url = "https://registro.br/cgi-bin/avail/";

        self::$referer = "Referer: http://registro.br/";



        $string = self::useCurl();



        return self::grabInformationFromString( $string );



    } // end of checkFromRegistroBR method





    /**
     * This method connecs to whois server
     *
     * @see useCurl()
     * @return String
     */

    public static function checkFromWhois(){



        /**
         * Values required for useCurl()
         * @see useCurl()
         */



        self::$postFields = null;

        //$postfields["d"] = self::$domain;

        //$postfields["tld"] = self::$extension;



        self::$url = "http://www.whois.net/whois/"

                        . self::$domain . '.' . self::$extension;



        self::$referer = "Referer: http://www.whois.net/";



        $string = self::useCurl();



        return self::grabInformationFromString( $string );



    } // enf of checkFromWhois method





} // end of the class
?>